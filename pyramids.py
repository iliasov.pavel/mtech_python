class PyramidCompressor:

    def __init__(self):
        self._pyramid_min_elems = 4
        self._pyramid__row_length_step = 2

    def _pyramid_rows_length_generator(self, pyramid_sequence):
        counter = 0
        row_length = 1
        while True:
            counter += row_length
            if counter > len(pyramid_sequence):
                break
            else:
                yield row_length
                row_length += self._pyramid__row_length_step

    def _get_pyramid_rows(self, pyramid_sequence):
        pyramid_rows = []
        slice_start = 0
        reversed_sequence = pyramid_sequence[::-1]
        for row_length in self._pyramid_rows_length_generator(
                pyramid_sequence=pyramid_sequence
        ):
            slice_end = slice_start + row_length
            row = reversed_sequence[slice_start:slice_end]
            pyramid_rows.append(row)
            slice_start = slice_end
        return pyramid_rows

    def _get_pyramid_triangles(self, pyramid_sequence):
        pyramid_rows = self._get_pyramid_rows(
            pyramid_sequence=pyramid_sequence
        )
        pyramid_triangles = []
        number_of_rows = len(pyramid_rows)
        pairs_of_rows = []
        for i in range(0, number_of_rows, 2):
            pairs_of_rows.append(pyramid_rows[i:i+2])
        for first_row, second_row in pairs_of_rows:
            first_row_triangles_elems = []
            slice_start = 0
            step = 1
            while True:
                slice_end = slice_start + step
                first_row_triangles_elems.append(
                    first_row[slice_start:slice_end]
                )
                slice_start = slice_end
                if slice_start >= len(first_row):
                    break
                step = 1 if step == 3 else 3
            second_row_triangles_elems = []
            slice_start = 0
            step = 3
            while True:
                slice_end = slice_start + step
                second_row_triangles_elems.append(
                    second_row[slice_start:slice_end]
                )
                slice_start = slice_end
                if slice_start >= len(first_row):
                    break
                step = 1 if step == 3 else 3
            for first_row_triangle_elems, second_row_triangle_elems in zip(
                    first_row_triangles_elems,
                    second_row_triangles_elems
            ):
                triangle_elems = first_row_triangle_elems + \
                                 second_row_triangle_elems
                pyramid_triangles.append(triangle_elems)
        return pyramid_triangles

    @staticmethod
    def _get_pyramid_compress_status(pyramid_triangles):
        status = True
        for triangle in pyramid_triangles:
            if len(set(triangle)) > 1:
                status = False
        return status

    @staticmethod
    def _compress_pyramid(pyramid_triangles):
        return ''.join([triangle[0] for triangle in pyramid_triangles[::-1]])

    def compress_pyramid_recursive(self, pyramid_sequence):
        if len(pyramid_sequence) < self._pyramid_min_elems:
            return pyramid_sequence
        pyramid_triangles = self._get_pyramid_triangles(
            pyramid_sequence=pyramid_sequence
        )
        compress_status = self._get_pyramid_compress_status(
            pyramid_triangles=pyramid_triangles
        )
        if compress_status:
            compressed_pyramid_sequence = self._compress_pyramid(
                pyramid_triangles=pyramid_triangles
            )
            return self.compress_pyramid_recursive(
                pyramid_sequence=compressed_pyramid_sequence
            )
        else:
            return pyramid_sequence


if __name__ == '__main__':

    compressor = PyramidCompressor()

    with open('./input.txt', 'r') as file:
        pyramid_sequence = file.readline()

    compressed_pyramid_sequence = compressor.compress_pyramid_recursive(
        pyramid_sequence=pyramid_sequence
    )

    with open('./output.txt', 'w') as file:
        file.write(compressed_pyramid_sequence)
